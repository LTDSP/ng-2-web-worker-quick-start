/**
 * Created by lucas on 31/10/2016.
 */
import { platformWorkerAppDynamic } from '@angular/platform-webworker-dynamic';

import { AppModule } from './app.module';

platformWorkerAppDynamic().bootstrapModule(AppModule);
