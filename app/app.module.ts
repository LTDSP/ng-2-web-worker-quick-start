import { NgModule }      from '@angular/core';

import { AppComponent }  from './app.component';
import {WorkerAppModule} from "@angular/platform-webworker";
import { FormsModule } from '@angular/forms';
import {WaveformComponent} from "./waveform/waveform.component";
import {MaterialModule} from "@angular/material";

@NgModule({
  imports:      [ WorkerAppModule, FormsModule, MaterialModule.forRoot() ],
  declarations: [
    AppComponent,
    WaveformComponent
  ],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }
