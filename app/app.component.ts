import {Component} from '@angular/core';
import {
  ServiceMessageBrokerFactory,
  PRIMITIVE
} from "@angular/platform-webworker";

@Component({
  selector: 'my-app',
  template: `
<!--<app-waveform></app-waveform>-->
<md-toolbar color="primary">
  <md-icon>face</md-icon>
  <!-- This fills the remaining space of the current row -->
  <span class="app-toolbar-filler"></span>

</md-toolbar>
<button (click)="longTask()">Calculate</button>
`
})
export class AppComponent {

  // audioBuffer: AudioBuffer = undefined;
  constructor(private serviceBrokerFactory: ServiceMessageBrokerFactory) {
    serviceBrokerFactory.createMessageBroker('ECHO', false)
      .registerMethod('echo', [PRIMITIVE], (val: string): Promise<string> => {
        return new Promise<string>((res, rej) => {
          try {
            res(val);
          } catch (e) {
            rej(e);
          }
        });
      }, PRIMITIVE);

    serviceBrokerFactory.createMessageBroker('AUDIO', false)
      .registerMethod('process', [PRIMITIVE], (buffer: Float32Array): Promise<Float32Array> => {
        return new Promise<Float32Array>(res => {
          buffer.forEach((n, i) => buffer[i] = n * 0.1);
          res(buffer);
        });
      }, PRIMITIVE);
  }

  longTask(): void {
    let test: number = 0;
    for (let i = 0; i < 44100 * 100000; ++i) {
      test = i * 2;
    }
    console.log(test);
  }
}
