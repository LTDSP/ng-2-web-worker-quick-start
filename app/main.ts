import {
  bootstrapWorkerUi, UiArguments,
  FnArg, PRIMITIVE, ClientMessageBroker, ClientMessageBrokerFactory
} from '@angular/platform-webworker';
import {PlatformRef} from "@angular/core";

interface AudioContextConstructor {
  new(): AudioContext
}

interface WindowAudioContext {
  AudioContext?: AudioContextConstructor;
  webkitAudioContext?: AudioContextConstructor
}

bootstrapWorkerUi('loader.js').then(afterBootstrap);

function afterBootstrap(ref: PlatformRef) {
  const brokerFactory: ClientMessageBrokerFactory = ref.injector.get(ClientMessageBrokerFactory);
  const broker: ClientMessageBroker = brokerFactory.createMessageBroker('ECHO', false);

  const factory: WindowAudioContext = (window as WindowAudioContext);
  const audioContext = new (factory.AudioContext || factory.webkitAudioContext)();
  const sampleRate: number = 44100.0;
  const durationSamples: number = 10 * sampleRate;

  let buffer: AudioBuffer = audioContext.createBuffer(1, durationSamples, sampleRate);
  let channel: Float32Array = buffer.getChannelData(0);

  const makeOsc = (freq: number, sampleRate: number): (n: number) => number => {
    return (n) => Math.sin(2.0 * (n / sampleRate) * freq * Math.PI);
  };
  const sine440: (n: number) => number = makeOsc(440.0, sampleRate);

  for (let n = 0 ; n < channel.length ; ++n) {
    channel[n] = sine440(n);
  }

  const sendMessage = () => {
    const args = new UiArguments('echo', [new FnArg('test echo', PRIMITIVE)]);
    broker.runOnService(args, PRIMITIVE).then((result: string) => {
      console.log(result);
    })
  };

  const audioBroker: ClientMessageBroker = brokerFactory.createMessageBroker('AUDIO', false);
  const doProcess = () => {
    const args = new UiArguments('process', [new FnArg(channel, PRIMITIVE)]);
    audioBroker.runOnService(args, PRIMITIVE).then((result: Float32Array) => {
      let sourceNode: AudioBufferSourceNode = audioContext.createBufferSource();
      buffer.getChannelData(0).set(result);
      sourceNode.buffer = buffer;
      sourceNode.connect(audioContext.destination);
      sourceNode.start();
    })
  };

  setInterval(sendMessage, 1000);
  setTimeout(doProcess, 5000);
}
