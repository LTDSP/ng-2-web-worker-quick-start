/**
 * Created by lucas on 31/10/2016.
 */
importScripts(
  "node_modules/core-js/client/shim.min.js",
  "node_modules/systemjs/dist/system-polyfills.js",
  "node_modules/zone.js/dist/zone.js",
  "node_modules/reflect-metadata/Reflect.js",
  "node_modules/systemjs/dist/system.src.js",
  "node_modules/waves-ui/waves-ui.min.js",
  "systemjs.config.js"
);

System.import("app/worker.main").catch(console.error);
